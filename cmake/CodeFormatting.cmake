find_program(FORMAT_CPP_PRG NAMES clang-format clang-format-6.0)

if(NOT FORMAT_CPP_PRG)
    message(WARNING "No program 'clang-format' found")
endif()

find_program(FORMAT_CMAKE_PRG cmake-format)

if(NOT FORMAT_CMAKE_PRG)
    message(WARNING "No program 'cmake-format' found")
endif()

set(FIND_CPP_COMMAND
    "find ${PROJECT_SOURCE_DIR} -regex '.*\\.\\(c\\|cc\\|cpp\\|cxx\\|h\\|hh\\|hpp\\|hxx\\)$' -not -path '${CMAKE_BINARY_DIR}/*' -not -path '${CMAKE_SOURCE_DIR}/submodules/*'"
)

set(FIND_CMAKE_COMMAND
    "find ${PROJECT_SOURCE_DIR} -type f \\( -iname CMakeLists.txt -o -iname \\*.cmake \\) -not -path '${CMAKE_BINARY_DIR}/*' -not -path '${CMAKE_SOURCE_DIR}/submodules/*'"
)

add_custom_target(
    codeformat
    VERBATIM
    COMMAND bash -c "${FIND_CPP_COMMAND} | xargs -n 1 ${FORMAT_CPP_PRG} -i"
    COMMAND bash -c "${FIND_CMAKE_COMMAND} | xargs -n 1 ${FORMAT_CMAKE_PRG} -i"
)

add_custom_target(
    formatcheck
    VERBATIM
    COMMAND
        bash -c
        "${FORMAT_CPP_PRG} --version; ${FIND_CPP_COMMAND} | xargs -n 1 -I {} bash -c 'diff -u {} <(${FORMAT_CPP_PRG} {})'"
    COMMAND
        bash -c
        "echo 'cmake-format version';${FORMAT_CMAKE_PRG} --version; ${FIND_CMAKE_COMMAND} | xargs -n 1 -I {} bash -c 'diff -u {} <(${FORMAT_CMAKE_PRG} {})'"
)
