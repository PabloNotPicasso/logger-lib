include_guard(GLOBAL)
include(FeatureSummary)

option(ENABLE_CODE_FORMATTING "Code formatting check" ON)
add_feature_info(
    "Perform code formatting check" ENABLE_CODE_FORMATTING
    "enables code formatting check and add additional build targets relevant to code formatting"
)

option(ENABLE_CODE_ANALYSIS "Static code analysis" ON)
add_feature_info(
    "Perform statis code analysys" ENABLE_CODE_ANALYSIS "perform statis code analysys of this repo"
)

option(ENABLE_CODE_COVERAGE "Enable code coverage" OFF)
add_feature_info(ENABLE_CODE_COVERAGE ENABLE_CODE_COVERAGE "Enable code coverage")
