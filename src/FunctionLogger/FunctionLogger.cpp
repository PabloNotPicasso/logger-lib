#include "FunctionLogger.h"
namespace logging {

FunctionLogger::FunctionLogger(const std::string& functionName)
    : m_functionName(functionName)
{
    std::string function_start = m_functionName + " {";
    Logger::Instance().logMessage(LogLvl::INFO, function_start);
}

FunctionLogger::~FunctionLogger()
{
    std::string function_end = "} " + m_functionName + " Exit";
    Logger::Instance().logMessage(LogLvl::INFO, function_end);
}

} // namespace logging
