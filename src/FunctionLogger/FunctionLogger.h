#pragma once
#include "Logger/Logger.h"

namespace logging {

class FunctionLogger {
public:
    FunctionLogger(const std::string& functionName);
    ~FunctionLogger();

private:
    std::string m_functionName;
};

} // namespace logging
