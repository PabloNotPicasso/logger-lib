#pragma once
#include "FunctionLogger/FunctionLogger.h"

#define LOGGER logging::Logger::Instance()
#define FUNCTION_LOGGER logging::FunctionLogger

#define LOG(LOG_LVL, ...) LOGGER.logMessage(logging::LogLvl::LOG_LVL, __VA_ARGS__)

#define LOG_INFO(...) LOG(INFO, __VA_ARGS__)
#define LOG_WARNING(...) LOG(WARNING, __VA_ARGS__)
#define LOG_ERROR(...) LOG(ERROR, __VA_ARGS__)

#define CONCAT(a,b) a##b
#define LOG_FUNCTION logging::FunctionLogger CONCAT(FunctionLogger_,__LINE__)(__FUNCTION__);
