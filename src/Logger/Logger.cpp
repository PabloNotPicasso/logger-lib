#include "Logger.h"

#include <iostream>

namespace logging {

void Logger::logMessage(const LogLvl& logLvl, const std::string& message)
{
    std::cout << getLogLvlName(logLvl) << ": " << message << std::endl;
}

} // namespace logging
