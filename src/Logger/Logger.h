#pragma once

#include "LogLvl/LogLvl.h"

namespace logging {

class Logger {
public:
    static Logger& Instance()
    {
        static Logger logger;
        return logger;
    }
    void logMessage(const LogLvl& logLvl, const std::string& message);

    template<typename... Args>
    void logVariadicMessage(Args...)
    {
    }

private:
    Logger() = default;

    // Deleted
    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;
};

} // namespace logging
