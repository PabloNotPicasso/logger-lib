#pragma once
#include <string>

namespace logging {

enum class LogLvl { INFO, WARNING, ERROR };

std::string getLogLvlName(const LogLvl& logLvl);

} // namespace logging
