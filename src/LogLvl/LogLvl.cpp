#include "LogLvl.h"

#include <unordered_map>

namespace logging {

namespace {
const std::unordered_map<LogLvl, std::string> LogLvl_String_map{
    {LogLvl::INFO, "INFO"},
    {LogLvl::WARNING, "WARNING"},
    {LogLvl::ERROR, "ERROR"},
};
}

std::string getLogLvlName(const LogLvl& logLvl)
{
    return LogLvl_String_map.at(logLvl);
}

} // namespace logging
